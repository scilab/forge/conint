// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [ low, up ] = conint_normvar ( varargin )
    // C.I. of the variance of a normal variable.
    //
    // Calling Sequence
    //   [ low, up ] = conint_normvar ( n , v )
    //   [ low, up ] = conint_normvar ( n , v , level )
    //   [ low, up ] = conint_normvar ( n , v , level , twosided )
    //
    // Parameters
    //   n : a 1-by-1 matrix of doubles, positive, integer value, the number of samples
    //   v : a 1-by-1 matrix of doubles, positive, the variance estimate
    //   level : a 1-by-1 matrix of doubles, the confidence level (default level = 1.-0.95=0.05). level is expected to be in the range [0.,0.5]
    // twosided : a 1-by-1 matrix of booleans, the side of the interval (default twosided = %t). If twosided is true, then low and up is a two-sided interval [low,up]. If twosided is false, then the intervals are [-inf,up] and [low,inf].
    //   low : a 1-by-1 matrix of doubles, the estimated lower bound
    //   up : a 1-by-1 matrix of doubles, the estimated upper bound
    //
    // Description
    // Computes a confidence interval of the variance of a normal variable, that is, 
    // computes <literal>v</literal> as the variance of <literal>x</literal> and 
    // computes confidence intervals for <literal>v</literal>.
    //
    // If <literal>twosided</literal> is true, then 
    // <literal>low</literal> and <literal>up</literal> are such that 
    // the variance <literal>var(X)</literal> is such that 
    //
    // <literal>
    // P(low < var < up) = 1-level
    // </literal>
    //
    // If <literal>twosided</literal> is false, then 
    // <literal>low</literal> and <literal>up</literal> are such that 
    // the variance <literal>var(X)</literal> is such that 
    //
    // <literal>
    // P(var(X) < up) = 1-level
    // </literal>
    //
    // and
    //
    // <literal>
    // P(low < var(X)) = 1-level
    // </literal>
    //
    // This function makes the assumption that the 
    // data in <literal>x</literal> has a normal distribution with 
    // expectation <literal>mu(X)</literal> and 
    // variance <literal>var(X)</literal>.
    //
    // We use a Chi-square distribution to compute the confidence interval.
    //
    // Examples
    // // Sheldon Ross, Example 7.3h, page 252
    // // Compute a 90% confidence interval for the 
    // // variance of a normal variable.
    // x = [0.123 0.124 0.126 0.120 0.130 ...
    // 0.133 0.125 0.128 0.124 0.126]
    // n = size(x,"*")
    // v = variance(x)
    // [ low, up ] = conint_normvar ( n , v , 1.-0.90 )
    // // Then P(7.264D-06 <= var(X) <= 36.96D-06) = 0.90
    // // Give the confidence interval for the standard 
    // // deviation
    // [sqrt(low) sqrt(v) sqrt(up)]
    //
    // // Example from Gilbert Saporta, 
    // // Section 13.5.3, "Variance d'une loi normale"
    // // Section 13.5.3.2, "m est inconnu"
    // // Get the C.I. for the variance of a normal 
    // // variable with n=30 and var(X)=12.
    // x = [6;2;2;8;12;6;6;13;..
    //     8;2;10;0;7;5;4;4;3;..
    // 	7;4;9;6;2;-3;5;7;2;4;5;2;2];
    // n = size(x,"*")
    // v = variance(x)
    // [ low, up ] = conint_normvar ( n , v , 1.-0.90 )
    // // Then P(8.177 < var(X) < 19.65) = 0.90
    // // The 90% C.I. is (8.177,19.65)
    // // Notice that Saporta gets (8.46,20.33).
    // // This is because Saporta uses n as the denominator 
    // // in the variance estimate, while we use (n-1).
    //
    // // Example from 
    // // "Probability and Statistics for Engineers and Scientists",
    // // Walpole, Myers, Myers, Ye
    // // Example 9.17:
    // x = [46.4, 46.1, 45.8, 47.0, 46.1, ..
    //   45.9, 45.8, 46.9, 45.2, 46.0];
    // n = size(x,"*")
    // v = variance(x)
    // [ low, up ] = conint_normvar ( n , v , 1.-0.95 )
    // // Then (0.135,0.953) is a 95% C.I. for the variance.
    //
    // Authors
    //   Copyright (C) 2011 - Michael Baudin
    //
    // Bibliography
    //    http://en.wikipedia.org/wiki/Confidence_interval
    //    "Introduction to probability and statistics for engineers and scientists", Sheldon Ross, Third Edition, 2004
    //    "Probabilités, Analyse de Données et Statistique", Gilbert Saporta, 2nd Ed., 2006
    // // "Probability and Statistics for Engineers and Scientists", Ronald Walpole, Raymond Myers, Sharon Myers, Keying Ye, Eight Edition, 2006

    [lhs, rhs] = argn();
    apifun_checkrhs ( "conint_normvar" , rhs , 2 : 4 )
    apifun_checklhs ( "conint_normvar" , lhs , 2 : 2 )
    //
    // Get arguments
    n = varargin(1)
    v = varargin(2)
    level = apifun_argindefault ( varargin , 3 , 1.-0.95 )
    twosided = apifun_argindefault ( varargin , 4 , %t )
    //
    // Check type
    apifun_checktype ( "conint_normvar" , n ,    "n" ,     1 , "constant" )
    apifun_checktype ( "conint_normvar" , v ,    "v" ,     2 , "constant" )
    apifun_checktype ( "conint_normvar" , level, "level" , 3 , "constant" )
    apifun_checktype ( "conint_normvar" , twosided, "twosided" , 4 , "boolean" )
    //
    // Check size
    apifun_checkscalar ( "conint_normvar" , n ,        "n" ,       1 )
    apifun_checkscalar ( "conint_normvar" , v ,        "v" ,       2 )
    apifun_checkscalar ( "conint_normvar" , level ,    "level" ,   3 )
    apifun_checkscalar ( "conint_normvar" , twosided , "twosided" , 4 )
    //
    // Check content
    apifun_checkgreq ( "conint_normvar" , n , "n" , 1 , 1. )
    apifun_checkflint ( "conint_normvar" , n , "n" , 1 )
    apifun_checkgreq ( "conint_normvar" , v , "v" , 2 , 0. )
    apifun_checkrange ( "conint_normvar" , level , "level" , 3 , 0. , 0.5 )
    //
    if ( twosided ) then
        q = level/2.
        p = 1.-q
    else
        q = level
        p = 1.-q
    end
    f = cdfchi("X",n-1,p,q)
    low = (n-1)*v/f
    if ( twosided ) then
        p = level/2.
        q = 1.-p
    else
        p = level
        q = 1.-p
    end
    f = cdfchi("X",n-1,p,q)
    up = (n-1)*v/f
endfunction
