// Copyright (C) 2010-2011 - Michael Baudin

// Updates the .xml files by deleting existing files and 
// creating them again from the .sci with help_from_sci.

//
cwd = get_absolute_file_path("update_help.sce");
mprintf("Working dir = %s\n",cwd);
//
// Generate the library help
mprintf("Updating conint\n");
helpdir = cwd;
funmat = [
  "conint_normmu"
  "conint_normmunumber"
  "conint_normvar"
  "conint_bernoullip"
  "conint_bernoullipnumber"
  ];
macrosdir = fullfile(cwd,"..","..","macros");
demosdir = [];
modulename = "conint";
helptbx_helpupdate ( funmat , helpdir , macrosdir , demosdir , modulename , %t );
