// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// From Sheldon Ross, Example 7.5a, p.262
// We experimented with 100 transistors :
// 80 transistors works.
// Compute a 95% confidence interval for p, 
// the probability that a transistor works.
pe = 80./100.;
n = 100;
[low, up] = conint_bernoullip ( pe , n , 1.-0.95 );
assert_checkalmostequal(low,0.7216014,1.e-5);
assert_checkalmostequal(up,0.8783986,1.e-5);
// Then (0.7216,0.8774) is a 95% C.I. for p.

// From Gilbert Saporta
// Section 13.5.4 Intervalle de confiance 
// pour une proportion p
n = 400;
pe = 0.36;
[low, up] = conint_bernoullip ( pe , n , 1.-0.95 );
assert_checkalmostequal(low,0.3129609,1.e-5);
assert_checkalmostequal(up,0.4070391,1.e-5);
// Then (0.31,0.41) is a 95% C.I. for p.

// Check default value of level
n = 400;
pe = 0.36;
[low, up] = conint_bernoullip ( pe , n );
assert_checkalmostequal(low,0.3129609,1.e-5);
assert_checkalmostequal(up,0.4070391,1.e-5);

// Try various methods
// From Statistics: problems and solutions
// Edward Eryl Bassett,J. M. Bremner,B. J. T. Morgan
// "4C Binomial and Poisson distributions"
// "4C3 Women investors in building societies"
// p.148
pe = 22./80.;
n = 80;
level = 1.-0.90;
twosided = %t;
[low, up] = conint_bernoullip ( pe , n , level , twosided );
// Then (0.193,0.357) is a 90% C.I. for p.
assert_checkalmostequal(low,0.1928859,1.e-5);
assert_checkalmostequal(up,0.3571141,1.e-5);
// The quadratic or Wilson (1927) interval :
method = 2;
[low, up] = conint_bernoullip ( pe , n , level , twosided , method );
assert_checkalmostequal(low,0.2012659,1.e-5);
assert_checkalmostequal(up,0.3634549,1.e-5);
// Then (0.201,0.363) is an approximate 90% C.I. for p.
// Invert the Binomial: the Clopper-Pearson interval
method = 3;
[low, up] = conint_bernoullip ( pe , n , level , twosided , method );
assert_checkalmostequal(low,0.1884600,1.e-5);
assert_checkalmostequal(up,0.3522612,1.e-5);
// Then (0.188,0.352) is an approximate 90% C.I. for p.
