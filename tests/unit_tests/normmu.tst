// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// Sheldon Ross, Example 7.3a, page 241
x = [5, 8.5, 12, 15, 7, 9, 7.5, 6.5, 10.5];
n = size(x,"*");
me = mean(x);
v = 4.;
[ low, up ] = conint_normmu ( n , me, v);
assert_checkalmostequal(me,9.,1.e-5);
assert_checkalmostequal(low,7.6933573,1.e-5);
assert_checkalmostequal(up,10.306643,1.e-5);
// Then P(7.69 <= mu <= 10.31) = 0.95

//
// Get one-sided intervals, Example 7.3b, p. 242
twosided = %f;
[ low, up ] = conint_normmu ( n , me, v,  1-0.95 , twosided );
assert_checkalmostequal(me,9.,1.e-5);
assert_checkalmostequal(low,7.9034309,1.e-5);
assert_checkalmostequal(up,10.096569,1.e-5);
// Then : P(7.903 <= mu) = 0.95    
//       P(mu <= 10.097) = 0.95    

// Example 7.3c, p. 244
// Two-sided 99% interval, expected = (7.28, 10.72)
[ low, up ] = conint_normmu ( n , me, v,  1.-0.99 );
assert_checkalmostequal(me,9.,1.e-5);
assert_checkalmostequal(low,7.2827805,1.e-5);
assert_checkalmostequal(up,10.71722,1.e-5);
// One-sided 99% intervals are (7.447,inf), (−inf, 10.553)
[ low, up ] = conint_normmu ( n , me, v,  1.-0.99 , %f );
assert_checkalmostequal(me,9.,1.e-5);
assert_checkalmostequal(low,7.4491014,1.e-5);
assert_checkalmostequal(up,10.550899,1.e-5);

// EXAMPLE 7.3e, p. 247
// The variance is unknown: use Student's T random variable
v = variance(x);
[ low, up ] = conint_normmu ( n , me, v,  1.-0.95 , [] , %f );
assert_checkalmostequal(me,9.,1.e-5);
assert_checkalmostequal(low,6.630806,1.e-5);
assert_checkalmostequal(up,11.369194,1.e-5);
// Then P(6.63<= mu <= 11.37) = 0.95


// EXAMPLE 7.3f, p.249
x = [54, 63, 58, 72, 49, 92, 70, 73, ..
    69, 104, 48, 66, 80, 64, 77];
n = size(x,"*");
me = mean(x);
v = variance(x);
// Two-sided :
[ low, up ] = conint_normmu ( n , me, v,  1.-0.95 , [] , %f );
assert_checkalmostequal(me,69.266667,1.e-5);
assert_checkalmostequal(low,60.866937,1.e-5);
assert_checkalmostequal(up,77.666397,1.e-5);
// Then P(60.865<= mu <=77.6683) = 0.95
// One-sided :
[ low, up ] = conint_normmu ( n , me, v,  1.-0.95 , %f , %f );
assert_checkalmostequal(me,69.266667,1.e-5);
assert_checkalmostequal(low,62.368764,1.e-5);
assert_checkalmostequal(up,76.164569,1.e-5);
// Then : P (mu <= 76.16) = 0.95
// Then : P (62.368 <= mu) = 0.95

// EXAMPLE 7.3g, Monte-Carlo simulation
grand("setsd",0);
U = grand(100,1,"def");
x = sqrt(1-U.^2);
n = size(x,"*");
me = mean(x);
v = variance(x);
[ low, up ] = conint_normmu ( n , me, v,  1.-0.95 , [] , %f );
// Exact integral is : 
assert_checkalmostequal(me,%pi/4,1.e-1);
assert_checktrue(me>low);
assert_checktrue(me<up);

// Check default value of level
x = [5, 8.5, 12, 15, 7, 9, 7.5, 6.5, 10.5];
n = size(x,"*");
me = mean(x);
v = variance(x);
[ low, up ] = conint_normmu ( n , me, v , [] , [] , %f );
assert_checkalmostequal(me,9.,1.e-5);
assert_checkalmostequal(low,6.630806,1.e-5);
assert_checkalmostequal(up,11.369194,1.e-5);
