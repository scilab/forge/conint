// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [ low, up ] = conint_bernoullip ( varargin )
    // C.I. of the probability of a Bernoulli variable.
    //
    // Calling Sequence
    //   [low, up] = conint_bernoullip ( pe , n )
    //   [low, up] = conint_bernoullip ( pe , n , level )
    //   [low, up] = conint_bernoullip ( pe , n , level , twosided )
    //   [low, up] = conint_bernoullip ( pe , n , level , twosided , method )
    //
    // Parameters
    //   pe : a 1-by-1 matrix of doubles, an estimate of the probability
    //   n : a 1-by-1 matrix of doubles, integer value, positive, the number of trials
    //   level : a 1-by-1 matrix of doubles, the confidence level (default level = 1.-0.95=0.05). level is expected to be in the range [0.,0.5]
    // twosided : a 1-by-1 matrix of booleans, the side of the interval (default twosided = %t). If twosided is true, then low and up is a two-sided interval [low,up]. If twosided is false, then the intervals are [-inf,up] and [low,inf].
    //   method : a 1-by-1 matrix of doubles, integer value, the method to estimate the C.I. Available values are method=1,2,3 (default method = 1). See below for details.
    //   low : a 1-by-1 matrix of doubles, the estimated lower bound
    //   up : a 1-by-1 matrix of doubles, the estimated upper bound
    //
    // Description
    // Computes a confidence interval of the probability 
    // <literal>p</literal> of a Bernoulli variable.
    //
    // This function makes the assumption that the data 
    // in <literal>x</literal> has a binomial distribution with 
    // parameters <literal>p</literal> and 
    // <literal>n</literal>.
    // In other words, we consider that the random variable is a "success" with probability 
    // <literal>p</literal> and is a "failure" with probability
    // <literal>1-p</literal>.
    // We assume that we have performed <literal>n</literal> trials 
    // of this experiment.
    // We assume that <literal>pe</literal> 
    // is the fraction of the <literal>n</literal> trials which are successes. 
    // This function computes confidence intervals for <literal>p</literal>.
    //
    // If <literal>twosided</literal> is true, then 
    // <literal>low</literal> and <literal>up</literal> are such that 
    // the probability <literal>p</literal> satisfies the equality
    //
    // <literal>
    // P(low < p < up) = 1-level
    // </literal>
    //
    // If <literal>twosided</literal> is false, then 
    // <literal>low</literal> and <literal>up</literal> are such that 
    // the probability <literal>p</literal> satisfies the equality 
    //
    // <literal>
    // P(p < up) = 1-level
    // </literal>
    //
    // and
    //
    // <literal>
    // P(low < p) = 1-level
    // </literal>
    //
    // The confidence interval that we compute is approximate. 
    // It is based on the Central Limit Theorem.
    // We use a Normal distribution to compute the confidence interval.
    //
    // If method=1, then approximates the Binomial distribution 
    // with the Normal distribution. 
    // Then computes approximate roots of the quadratic equation.
    // This is the "Textbook" formula.
    //
    // If method=2, then approximates the Binomial distribution with the Normal distribution. 
    // Then computes exact roots of the quadratic equation.
    // This is the method by Wilson (1927).
    //
    // If method=3, then invert the quantiles of the 
    // Binomial distribution with the Normal distribution. 
    // This is the Clopper-Pearson interval.
    //
    // Examples
    // // From Sheldon Ross, Example 7.5a, p.262
    // // We experimented with 100 transistors :
    // // 80 transistors works.
    // // Compute a 95% confidence interval for p, 
    // // the probability that a transistor works.
    // pe = 80./100.;
    // n = 100;
    // [low, up] = conint_bernoullip ( pe , n , 1.-0.95 )
    // // Then (0.7216,0.8774) is an approximate 95% C.I. for p.
    //
    // // From Gilbert Saporta
    // // Section 13.5.4 Intervalle de confiance 
    // // pour une proportion p
    // n = 400;
    // pe = 0.36;
    // [low, up] = conint_bernoullip ( pe , n , 1.-0.95 )
    // // Then (0.31,0.41) is an approximate 95% C.I. for p.
    //
    // // Try various methods
    // // From Statistics: problems and solutions
    // // Edward Eryl Bassett,J. M. Bremner,B. J. T. Morgan
    // // "4C Binomial and Poisson distributions"
    // // "4C3 Women investors in building societies"
    // // p.148
    // pe = 22./80.;
    // n = 80;
    // level = 1.-0.90
    // twosided = %t
    // [low, up] = conint_bernoullip ( pe , n , level , twosided )
    // // Then (0.193,0.357) is a 90% C.I. for p.
    // // The quadratic or Wilson (1927) interval :
    // method = 2;
    // [low, up] = conint_bernoullip ( pe , n , level , twosided , method )
    // // Then (0.201,0.363) is an approximate 90% C.I. for p.
    // // Invert the Binomial: the Clopper-Pearson interval
    // method = 3
    // [low, up] = conint_bernoullip ( pe , n , level , twosided , method )
    // // Then (0.188,0.352) is an approximate 90% C.I. for p.
    //
    // Authors
    //   Copyright (C) 2011 - Michael Baudin
    //
    // Bibliography
    //    http://en.wikipedia.org/wiki/Confidence_interval
    //    "Introduction to probability and statistics for engineers and scientists", Sheldon Ross, Third Edition, 2004
    //    "Probabilités, Analyse de Données et Statistique", Gilbert Saporta, 2nd Ed., 2006
    //    http://en.wikipedia.org/wiki/Binomial_proportion_confidence_interval

    [lhs, rhs] = argn();
    apifun_checkrhs ( "conint_bernoullip" , rhs , 2 : 5 )
    apifun_checklhs ( "conint_bernoullip" , lhs , 2 : 2 )
    //
    // Get arguments
    pe = varargin(1)
    n = varargin(2)
    level = apifun_argindefault ( varargin , 3 , 1.-0.95 )
    twosided = apifun_argindefault ( varargin , 4 , %t )
    method = apifun_argindefault ( varargin , 5 , 1 )
    //
    // Check type
    apifun_checktype ( "conint_bernoullip" , pe ,   "pe" ,    1 , "constant" )
    apifun_checktype ( "conint_bernoullip" , n ,    "n" ,     2 , "constant" )
    apifun_checktype ( "conint_bernoullip" , level, "level" , 3 , "constant" )
    apifun_checktype ( "conint_bernoullip" , twosided, "twosided" , 4 , "boolean" )
    apifun_checktype ( "conint_bernoullip" , method, "method" , 5 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "conint_bernoullip" , pe ,      "pe" ,      1 )
    apifun_checkscalar ( "conint_bernoullip" , n ,       "n" ,       2 )
    apifun_checkscalar ( "conint_bernoullip" , level ,   "level" ,   3 )
    apifun_checkscalar ( "conint_bernoullip" , twosided ,   "twosided" ,   4 )
    apifun_checkscalar ( "conint_bernoullip" , method ,   "method" ,   5 )
    //
    // Check content
    apifun_checkrange ( "conint_bernoullip" , pe , "pe" , 1 , 0. , 1. )
    apifun_checkgreq ( "conint_bernoullip" , n , "n" , 2 , 1 )
    apifun_checkflint ( "conint_bernoullip" , n , "n" , 2 )
    apifun_checkrange ( "conint_bernoullip" , level , "level" , 3 , 0. , 0.5 )
    apifun_checkoption ( "conint_bernoullip" , method , "method" , 5 , [1,2,3] )
    //
    select method
    case 1 then 
        // The Normal approximation to the Binomial.
        // Moreover, computes approximate roots of the quadratic 
        // polynomial.
        if ( twosided ) then
            q = level/2.
            p = 1.-q
        else
            q = level
            p = 1.-q
        end
        f = cdfnor("X",0.,1.,p,q)
        low = pe - f * sqrt(pe*(1.-pe)/n)
        up = pe + f * sqrt(pe*(1.-pe)/n)
    case 2 then
        // The quadratic or Wilson (1927) interval :
        // computes the exact roots of the quadratic polynomial 
        // which appears when approximating the Binomial distribution 
        // with a Normal distribution.
        if ( twosided ) then
            q = level/2.
            p = 1.-q
        else
            q = level
            p = 1.-q
        end
        f = cdfnor("X",0.,1.,p,q)
        c = []
        c(3) = pe^2
        c(2) = -(f^2/n + 2*pe)
        c(1) = 1+f^2/n
        proot = roots(c)
        if (or(imag(proot)<>0.)) then
            error(msprintf("%s: Complex roots (%s,%s).",..
            "conint_bernoullip",string(proot(1)),,string(proot(2))))
        end
        proot = real(proot)
        low = min(proot)
        up = max(proot)
    case 3 then
        // Invert the Binomial: the Clopper-Pearson interval
        if ( twosided ) then
            q = level/2.
            p = 1.-q
        else
            q = level
            p = 1.-q
        end
        S=cdfbin("S",n,pe,1.-pe,p,q)
        up = S/n
        if ( twosided ) then
            p = level/2.
            q = 1.-p
        else
            p = level
            q = 1.-p
        end
        S=cdfbin("S",n,pe,1.-pe,p,q)
        low = S/n
    else 
        error(msprintf("%s: Unknown method %s.","conint_bernoullip",string(method)))
    end

endfunction
