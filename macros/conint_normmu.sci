// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [ low, up ] = conint_normmu ( varargin )
    // C.I. of the mean of a normal variable.
    //
    // Calling Sequence
    //   [ low, up ] = conint_normmu ( n , me , v )
    //   [ low, up ] = conint_normmu ( n , me , v , level )
    //   [ low, up ] = conint_normmu ( n , me , v , level , twosided )
    //   [ low, up ] = conint_normmu ( n , me , v , level , twosided , known )
    //
    // Parameters
    //   n : a n-by-1 or 1-by-n matrix of doubles, integer value, positive, the number of samples
    //   me : a 1-by-1 matrix of doubles, the mean estimate
    //   level : a 1-by-1 matrix of doubles, the confidence level (default level = 1.-0.95=0.05). level is expected to be in the range <literal>[0.,0.5]</literal>
    //   v : a 1-by-1 matrix of doubles, the variance. By default, the variance is assumed to be exact (i.e. by default known=%t). If the variance <literal>v</literal> is estimated, then set the <literal>known</literal> option to %f.
    // twosided : a 1-by-1 matrix of booleans, the side of the interval (default twosided = %t). If <literal>twosided</literal> is %t, then <literal>low</literal> and <literal>up</literal> make a two-sided interval <literal>[low,up]</literal>. If <literal>twosided</literal> is %f, then the intervals are <literal>[-inf,up]</literal> and <literal>[low,inf]</literal>.
    // known : a 1-by-1 matrix of booleans, %t if the variance <literal>v</literal> is exact (default known = %t). If <literal>known</literal> is %t, then <literal>v</literal> is the exact variance. If known is %f, then <literal>v</literal> is the estimate of the variance.
    //   low : a 1-by-1 matrix of doubles, the estimated lower bound
    //   up : a 1-by-1 matrix of doubles, the estimated upper bound
    //
    // Description
    // Computes a confidence interval of the mean of a normal variable, that is, 
    // computes <literal>m</literal> as the mean of <literal>x</literal> and 
    // computes confidence intervals for <literal>mu(X)</literal>.
    //
    // If <literal>twosided</literal> is true, then 
    // <literal>low</literal> and <literal>up</literal> are such that 
    // the expectation <literal>mu(X)</literal> is such that 
    //
    // <literal>
    // P(low < mu(X) < up) = 1-level
    // </literal>
    //
    // If <literal>twosided</literal> is false, then 
    // <literal>low</literal> and <literal>up</literal> are such that 
    // the expectation <literal>mu</literal> is such that 
    //
    // <literal>
    // P(mu(X) < up) = 1-level
    // </literal>
    //
    // and
    //
    // <literal>
    // P(low < mu(X)) = 1-level
    // </literal>
    //
    // This function makes the assumption that the 
    // data in x has a normal distribution with expectation <literal>mu(X)</literal> and 
    // variance <literal>var(X)</literal>.
    //
    // If the variance is exact (i.e. <literal>known=%t</literal>)
    // then we use a Normal distribution to compute the confidence interval.
    //
    // If the variance is estimated (i.e. if <literal>known=%f</literal>), 
    // then we use Student's T distribution to compute the confidence interval.
    //
    // Examples
    // // Sheldon Ross, Example 7.3a, page 241
    // x = [5, 8.5, 12, 15, 7, 9, 7.5, 6.5, 10.5]
    // n = size(x,"*")
    // me = mean(x)
    // // By default, we compute a two-sided 95% C.I. 
    // // with exact variance:
    // v = 4.
    // [ low, up ] = conint_normmu ( n , me , v )
    // low_expected = 7.69
    // up_expected = 10.31
    // // Then P(7.69 <= mu <= 10.31) = 0.95
    //
    // // Get one-sided 95% C.I.
    // // Example 7.3b, p. 242
    // low_expected = 7.903
    // up_expected = 10.097
    // twosided = %f
    // [ low, up ] = conint_normmu ( n , me , v , [] , twosided )
    // // Then : P(7.903 <= mu) = 0.95    
    // //       P(mu <= 10.097) = 0.95    
    //
    // // Example 7.3c, p. 244
    // // Two-sided 99% interval : (7.28, 10.72)
    // [ low, up ] = conint_normmu ( n , me , v , 1.-0.99 )
    // // One-sided 99% intervals : (7.447,inf), (−inf, 10.553)
    // [ low, up ] = conint_normmu ( n , me , v , 1.-0.99 , %f )
    //
    // // EXAMPLE 7.3e, p. 247
    // // The variance is unknown: use Student's T random variable
    // v = variance(x)
    // [ low, up ] = conint_normmu ( n , me , v , [] , [] , %f )
    // // Then P(6.63<= mu <= 11.37) = 0.95
    //
    // // EXAMPLE 7.3f, p.249
    // x = [54, 63, 58, 72, 49, 92, 70, 73, ..
    //     69, 104, 48, 66, 80, 64, 77];
    // n = size(x,"*")
    // me = mean(x)
    // v = variance(x)
    // // Two-sided :
    // [ low, up ] = conint_normmu ( n , me , v , 1.-0.95 , [] , %f )
    // // Then P(60.865<= mu <=77.6683) = 0.95
    // // One-sided :
    // [ low, up ] = conint_normmu ( n , me , v , 1.-0.95 , %f , %f )
    // // Then : P (mu <= 76.16) = 0.95
    // // Then : P (62.368 <= mu) = 0.95
    //
    // // EXAMPLE 7.3g, Monte-Carlo simulation
    // U = grand(100,1,"def");
    // x = sqrt(1-U.^2);
    // n = size(x,"*")
    // me = mean(x)
    // v = variance(x)
    // [ low, up ] = conint_normmu ( n , me , v , [] , [] , %f )
    // // Exact integral is : 
    // %pi/4
    //
    // Authors
    //   Copyright (C) 2011 - Michael Baudin
    //
    // Bibliography
    //    http://en.wikipedia.org/wiki/Confidence_interval
    //    "Introduction to probability and statistics for engineers and scientists", Sheldon Ross, Third Edition, 2004

    [lhs, rhs] = argn();
    apifun_checkrhs ( "conint_normmu" , rhs , 3 : 6 )
    apifun_checklhs ( "conint_normmu" , lhs , 2 : 2 )
    //
    // Get arguments
    n = varargin(1)
    me = varargin(2)
    v = varargin(3)
    level = apifun_argindefault ( varargin , 4 , 1.-0.95 )
    twosided = apifun_argindefault ( varargin , 5 , %t )
    known = apifun_argindefault ( varargin , 6 , %t )
    //
    // Check type
    apifun_checktype ( "conint_normmu" , n ,    "n" ,     1 , "constant" )
    apifun_checktype ( "conint_normmu" , me ,    "me" ,   2 , "constant" )
    apifun_checktype ( "conint_normmu" , v, "v" , 3 , "constant" )
    apifun_checktype ( "conint_normmu" , level, "level" , 4 , "constant" )
    apifun_checktype ( "conint_normmu" , twosided, "twosided" , 5 , "boolean" )
    apifun_checktype ( "conint_normmu" , known, "known" , 6 , "boolean" )
    //
    // Check size
    apifun_checkscalar ( "conint_normmu" ,  n ,       "n" , 1 )
    apifun_checkscalar ( "conint_normmu" , me ,      "me" , 2 )
    apifun_checkscalar ( "conint_normmu" ,  v ,       "v" , 3 )
    apifun_checkscalar ( "conint_normmu" , level ,   "level" ,   4 )
    apifun_checkscalar ( "conint_normmu" , twosided ,   "twosided" ,   5 )
    apifun_checkscalar ( "conint_normmu" , known ,   "known" ,   6 )
    //
    // Check content
    apifun_checkflint ( "conint_normmu" ,   n ,       "n" , 1 )
    apifun_checkgreq ( "conint_normmu" ,    n ,       "n" , 1 , 1. )
    tiniest = number_properties("tiniest")
    apifun_checkgreq ( "conint_normmu" , v , "v" , 3 , tiniest )
    apifun_checkrange ( "conint_normmu" , level , "level" , 4 , 0. , 0.5 )
    //
    if ( twosided ) then
        q = level/2.
        p = 1.-q
    else
        q = level
        p = 1.-q
    end
    s = sqrt(v)
    if ( known ) then
        // The variance is known:
        // use a Standard Normal variable
        f = cdfnor("X",0.,1.,p,q)
    else
        // The variance is unknown:
        // use Student's T random variable.
        f = cdft("T",n-1,p,q)
    end
    low = me - f * s/sqrt(n)
    up = me + f * s/sqrt(n)
endfunction
