<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from conint_normmunumber.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="conint_normmunumber" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>conint_normmunumber</refname><refpurpose>Number of experiments for a Normal variable.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   n = conint_normmunumber ( len , v )
   n = conint_normmunumber ( len , v , level )
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>len :</term>
      <listitem><para> a 1-by-1 matrix of doubles, positive, the length of the confidence interval.</para></listitem></varlistentry>
   <varlistentry><term>v :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the variance</para></listitem></varlistentry>
   <varlistentry><term>level :</term>
      <listitem><para> a 1-by-1 matrix of doubles, the confidence level (default level = 1.-0.95=0.05). level is expected to be in the range [0.,0.5]</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, the number of experiments</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns the number of experiments which guarantees that the
length of the confidence interval is less than <literal>len</literal>.
This two-sided confidence interval is for the expectation of the
random variable and we make the assumption that the random
variable has a Normal distribution.
   </para>
   <para>
In other words, if <literal>x</literal> is a vector
with <literal>n</literal> entries, then
   </para>
   <para>
<screen>
[ m, low, up ] = conint_normmu ( x , level , v )
</screen>
   </para>
   <para>
is so that
   </para>
   <para>
<literal>
abs(up-low) &lt;= len
</literal>
   </para>
   <para>
To compute <literal>n</literal>, we assume that the random variable
has Normal distribution with known variance <literal>v</literal>.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Sheldon Ross, EXAMPLE 7.3d, p245
// Compute the number of experiments for a random variable
// with standard deviation 0.3 so that
// the expectation has a two-sided 95% confidence interval
// with length no greater than 0.2.
n = conint_normmunumber ( 0.2 , 0.3^2 , 1.-0.95 )
// Expected: n=35

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2011 - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>http://en.wikipedia.org/wiki/Confidence_interval</para>
   <para>"Introduction to probability and statistics for engineers and scientists", Sheldon Ross, Third Edition, 2004</para>
</refsection>
</refentry>
