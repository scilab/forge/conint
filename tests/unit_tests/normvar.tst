// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// Sheldon Ross, Example 7.3h, page 252
// Compute a 90% confidence interval for the 
// variance of a normal variable.
x = [0.123 0.124 0.126 0.120 0.130 ...
0.133 0.125 0.128 0.124 0.126];
n = size(x,"*");
v = variance(x);
[ low, up ] = conint_normvar ( n , v , 1.-0.90 );
assert_checkalmostequal(v,1.36555556D-05,1.e-5);
assert_checkalmostequal(low,7.26403231D-06,1.e-5);
assert_checkalmostequal(up,3.69611516D-05,1.e-5);
// Then P(7.26403231D-06 <= var(X) <= 36.9611516D-06) = 0.90

// Example from Gilbert Saporta, 
// Section 13.5.3, "Variance d'une loi normale"
// Section 13.5.3.2, "m est inconnu"
x = [6;2;2;8;12;6;6;13;..
    8;2;10;0;7;5;4;4;3;..
	7;4;9;6;2;-3;5;7;2;4;5;2;2];
n = size(x,"*");
v = variance(x);
[ low, up ] = conint_normvar ( n , v , 1.-0.90 )
assert_checkalmostequal(v,12.,1.e-5);
assert_checkalmostequal(low,8.1772743,1.e-5);
assert_checkalmostequal(up,19.651728,1.e-5);

// Check default value of level
x = [0.123 0.124 0.126 0.120 0.130 ...
0.133 0.125 0.128 0.124 0.126];
n = size(x,"*");
v = variance(x);
[ low, up ] = conint_normvar ( n , v );
assert_checkalmostequal(v,1.36555556D-05,1.e-5);
assert_checkalmostequal(low,6.46067919D-06,1.e-5);
assert_checkalmostequal(up,4.55119530D-05,1.e-5);
