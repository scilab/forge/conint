// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// From Gilbert Saporta, 
// Section 13.5.4 Intervalle de confiance 
// pour une proportion p
// p.313
len = 0.05*2;
level = 1.-0.90;
n = conint_bernoullipnumber ( len , level );
assert_checkequal(n,271);

// Reproduce the table p 313.
mprintf("level = %9s     %9s %9s %9s", ..
 " ", "1.-0.90", "1.-0.95", "1.-0.98")
for len = 2*[0.01 0.02 0.05]
    mprintf("len = %9s, n = ", string(len))
    for level = 1.-[0.90 0.95 0.98]
        n = conint_bernoullipnumber ( len , level );
        mprintf("%9s ", string(n));
    end
    mprintf("\n");
end

// From Sheldon Ross,
// Example 7.5c
len = 0.05;
// Compute n for 95% C.I.
n = conint_bernoullipnumber ( len );
assert_checkequal(n,1537);
// Compute n for 99% C.I.
level = 1.-0.99;
n = conint_bernoullipnumber ( len , level );
assert_checkequal(n,2654);
// Compute n for 99% C.I. and probability estimate
pe = 26. / 30.;
n = conint_bernoullipnumber ( len , level , pe );
assert_checkequal(n,1227);
