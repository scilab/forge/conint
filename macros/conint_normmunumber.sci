// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function n = conint_normmunumber ( varargin )
    // Number of experiments for a Normal variable.
    //
    // Calling Sequence
    //   n = conint_normmunumber ( len , v )
    //   n = conint_normmunumber ( len , v , level )
    //
    // Parameters
    //   len : a 1-by-1 matrix of doubles, positive, the length of the confidence interval.
    //   v : a 1-by-1 matrix of doubles, the variance
    //   level : a 1-by-1 matrix of doubles, the confidence level (default level = 1.-0.95=0.05). level is expected to be in the range [0.,0.5]
    //   n : a 1-by-1 matrix of doubles, integer value, the number of experiments
    //
    // Description
    // Returns the number of experiments which guarantees that the 
    // length of the confidence interval is less than <literal>len</literal>. 
    // This two-sided confidence interval is for the expectation of the 
    // random variable and we make the assumption that the random 
    // variable has a Normal distribution.
    //
    // In other words, if <literal>x</literal> is a vector 
    // with <literal>n</literal> entries, then 
    //
    // <screen>
    // [ m, low, up ] = conint_normmu ( x , level , v )
    // </screen>
    //
    // is so that 
    //
    // <literal>
    // abs(up-low) <= len
    // </literal>
    //
    // To compute <literal>n</literal>, we assume that the random variable 
    // has Normal distribution with known variance <literal>v</literal>. 
    //
    // Examples
    // // Sheldon Ross, EXAMPLE 7.3d, p245
    // // Compute the number of experiments for a random variable 
    // // with standard deviation 0.3 so that 
    // // the expectation has a two-sided 95% confidence interval 
    // // with length no greater than 0.2.
    // n = conint_normmunumber ( 0.2 , 0.3^2 , 1.-0.95 )
    // // Expected: n=35
    //
    // Authors
    //   Copyright (C) 2011 - Michael Baudin
    //
    // Bibliography
    //    http://en.wikipedia.org/wiki/Confidence_interval
    //    "Introduction to probability and statistics for engineers and scientists", Sheldon Ross, Third Edition, 2004

    [lhs, rhs] = argn();
    apifun_checkrhs ( "conint_normmunumber" , rhs , 2 : 3 )
    apifun_checklhs ( "conint_normmunumber" , lhs , 0 : 1 )
    //
    // Get arguments
    len = varargin(1)
    v = varargin(2)
    level = apifun_argindefault ( varargin , 3 , 1.-0.95 )
    //
    // Check type
    apifun_checktype ( "conint_normmunumber" , len ,    "len" , 1 , "constant" )
    apifun_checktype ( "conint_normmunumber" , v, "v" , 2 , "constant" )
    apifun_checktype ( "conint_normmunumber" , level, "level" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "conint_normmunumber" , len ,   "len" ,   1 )
    apifun_checkscalar ( "conint_normmunumber" , v ,   "v" ,   2 )
    apifun_checkscalar ( "conint_normmunumber" , level ,   "level" ,   3 )
    //
    // Check content
    apifun_checkgreq ( "conint_normmunumber" , len , "len" , 1 , 0. )
    apifun_checkgreq ( "conint_normmunumber" , v , "v" , 2 , 0. )
    apifun_checkrange ( "conint_normmunumber" , level , "level" , 3 , 0. , 0.5 )
    //
    p = (2-level)/2
    q = level/2
    f = cdfnor("X",0.,1.,p,q)
    n = v * (2.*f/len)^2
    n = ceil(n)
endfunction
