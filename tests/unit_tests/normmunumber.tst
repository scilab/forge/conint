// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

// Sheldon Ross, Example 7.3a, page 241
n = conint_normmunumber ( 0.2 , 0.3^2 , 1.-0.95 );
assert_checkalmostequal(n,35);

// Check default value of level
n = conint_normmunumber ( 0.2 , 0.3^2 );
assert_checkalmostequal(n,35);
